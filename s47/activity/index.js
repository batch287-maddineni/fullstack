// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific object (HTML element) from the document. (webpage)

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// Alternatively, we can use the getElement functions to retrieve the element
	// document.getElementById
	// document.getElementByClassName
	// document.getElementByTagName


// Whenever a user interacts with a web page, this action is considered as an event
// The "addEventListener" is function that takes two arguements,
	// string identifying the event (keyup)
	// function that the listener will execute once the specified event is triggered(event)
txtFirstName.addEventListener('keyup', (event) => {
	// The innerHTML property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value+" "+txtLastName.value;
});

txtLastName.addEventListener('keyup', (event) => {
	// The innerHTML property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value+" "+txtLastName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	// The "event.target" contains the element where the event happened
	console.log(event.target);
	// The "event.tatgert.value" gets the value of the input object (similar to the textFirstName.value);
	console.log(event.target.value);
});